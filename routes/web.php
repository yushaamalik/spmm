<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Route::get('/', function () {
//     return view('welcome');
// });

Route::get('/', 'FrontEndController@index')->name('frontend.index');

Auth::routes();

Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');

//Events
Route::get('/events/listing', 'EventsController@listing')->name('events.listing')->middleware('auth');
Route::get('/events/create', 'EventsController@create')->name('events.create')->middleware('auth');
Route::post('/events/save', 'EventsController@store')->name('events.save')->middleware('auth');
Route::get('/events/view/{id}', 'EventsController@view')->name('events.view')->middleware('auth');
Route::get('/events/change-status/{id}', 'EventsController@changeStatus')->name('events.changeStatus')->middleware('auth');
Route::get('/events/edit/{id}', 'EventsController@edit')->name('events.edit')->middleware('auth');
Route::put('/events/update/{id}', 'EventsController@update')->name('events.update')->middleware('auth');
