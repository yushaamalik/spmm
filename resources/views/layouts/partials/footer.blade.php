<footer class="footer">
    <small>&copy; {{ date("Y") }} <a href="https://www.pitb.gov.pk/" target="_blank">Punjab Information Technology Board</a></small>
</footer>
