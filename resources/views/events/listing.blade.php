@extends('layouts.master')
@section('css')
<link href="{{asset('velzon/assets/libs/datatables.net-bs4/css/dataTables.bootstrap4.min.css')}} rel="stylesheet" type="text/css"/>
<link href="{{asset('velzon/assets/libs/datatables.net-buttons-bs4/css/buttons.bootstrap4.min.css')}}" rel="stylesheet"  type="text/css" />

<!-- Responsive datatable examples -->
<link href="{{asset('velzon/assets/libs/datatables.net-responsive-bs4/css/responsive.bootstrap4.min.css"')}}" rel="stylesheet"  type="text/css"  />
<style type="text/css">

</style>
@endsection
        <!-- ========== App Menu ========== -->
        @section('navigation')
        @include('layouts.navigation')
        @endsection
        @section('content')
        <!-- Left Sidebar End -->
        <!-- Vertical Overlay-->
        <div class="vertical-overlay"></div>

        <!-- ============================================================== -->
        <!-- Start right Content here -->
        <!-- ============================================================== -->
        <div class="main-content">

            <div class="page-content">
                <div class="container-fluid">

                    <!-- start page title -->
                    <div class="row">
                        <div class="col-12">
                            <div class="page-title-box d-sm-flex align-items-center justify-content-between">
                                <h4 class="mb-sm-0">Event</h4>

                                <div class="page-title-right">
                                    <ol class="breadcrumb m-0">
                                        <li class="breadcrumb-item"><a href="javascript: void(0);">Event</a></li>
                                        <li class="breadcrumb-item active">Listing</li>
                                    </ol>
                                </div>

                            </div>
                        </div>
                    </div>
                    <!-- end page title -->
                    @include('layouts.partials.alerts')
                    <div class="card">
                        <div class="card-header align-items-center d-flex">
                            <label><h4 class="card-title mb-0 flex-grow-1">Events Listing</h4></label>
                        </div>
                        <div class="card-body">
                            <form id="filter_data" name="filter_data">
                                <div class="form-row">
                                    <div class="form-group col-md">
                                        <div class="row">
                                            <h4>Filters</h4>
                                            
                                            <div class="col-md-2 mb-2">
                                                <label>Status</label>
                                                <select id="status1" name="status" class=" select2 form-control">
                                                    <option value="" selected >Select Status</option>
                                                    <option value="1">Active</option>
                                                    <option value="2">Non-Active</option>
                                                </select>
                                            </div>

                                            <div class="col-md-2 mb-3">
                                                <label>Date From</label>
                                                <input type="date" placeholder="Date From" name="date_from" id="date_from" class="form-control">
                                            </div>
                                            <div class="col-md-2 mb-3">
                                                <label>Date To</label>
                                                <input type="date" placeholder="Date To" name="date_to" id="date_to" class="form-control">
                                            </div>
                                           
                                            <div class="col-md-4 mb-3 mt-4">
                                                <button type="submit" id="submit_filter" class="btn btn-primary">Submit</button>
                                                <button id="reset_filter" class="btn btn-warning">Reset</button>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </form>
                            <div class="table-responsive">
                                    <table id="datatable" class="table table-bordered table-responsive">
                                            <thead class="table-primary">
                                                <tr>
                                                    <th>#</th>
                                                    <th>Name</th>
                                                    <th>Date From</th>
                                                    <th>Date To</th>
                                                    <th>Event Type</th>
                                                    <th>Action</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                            </tbody>
                                    </table>
                            </div>
                        </div>
                    </div>  
                            
            <!-- container-fluid -->
        </div>
        <!-- End Page-content -->

        
    </div>
    <!-- end main content-->

</div>
<!-- END layout-wrapper -->



<!--start back-to-top-->
<button onclick="topFunction()" class="btn btn-danger btn-icon" id="back-to-top">
    <i class="ri-arrow-up-line"></i>
</button>
<!--end back-to-top-->


@endsection
@section('js')
<script src="{{asset('velzon/assets/libs/datatable/js/jquery.dataTables.min.js')}}"></script>
<script src="{{asset('velzon/assets/libs/datatable/js/dataTables.bootstrap5.min.js')}}"></script>
<script>
        $(document).ready(function() {
            callingDataTable();
            $('.dataTables_filter').addClass('float-end');
        });
</script>
<script>
function callingDataTable(){
    var testTable = $('#datatable').DataTable({
            destroy: true,
            processing: true,
            serverSide: true,
            rowId: 'id',
            sClass:'text-center',
            ajax: {
                url: '{{ route('events.listing') }}',
                data:function(d){
                    d.status        = $('#status1').val();
                    d.date_from     = $('#date_from').val();
                    d.date_to       = $('#date_to').val();
                    
                }
               
            },
            columns: [
                {data: 'DT_RowIndex', searchable: false, orderable: false},
                {data: 'name', name: 'name', searchable: true},
                {data: 'date_from', name: 'date_from', searchable: true},
                {data: 'date_to', name: 'date_to', searchable: true},
                {data: 'event_type', name: 'event_type', searchable: true},
                {data: 'action', name: 'action'},
            ]
         });
         testTable.draw();
         $('#submit_filter').click(function(e){
            e.preventDefault();
            testTable.draw();
        });
        $(document).on('click','#reset_filter',function(e){
        e.preventDefault();
            $('#filter_data').trigger("reset");
            $('select[name="status"]').append('<option value="" hidden selected > Status </option>');
            $('select[name="date_from"]').append('<option value="" hidden selected > Status </option>');
            $('select[name="date_to"]').append('<option value="" hidden selected > Status </option>');
            $('#datatable').DataTable().ajax.reload();

        });
    }
</script>
@endsection
