@extends('layouts.master')
        <!-- ========== App Menu ========== -->
        @section('navigation')
        @include('layouts.navigation')
        @endsection
        @section('content')
        <!-- Left Sidebar End -->
        <!-- Vertical Overlay-->
        <div class="vertical-overlay"></div>

        <!-- ============================================================== -->
        <!-- Start right Content here -->
        <!-- ============================================================== -->
        <div class="main-content">

            <div class="page-content">
                <div class="container-fluid">

                    <!-- start page title -->
                    <div class="row">
                        <div class="col-12">
                            <div class="page-title-box d-sm-flex align-items-center justify-content-between">
                                <h4 class="mb-sm-0">Event</h4>

                                <div class="page-title-right">
                                    <ol class="breadcrumb m-0">
                                        <li class="breadcrumb-item"><a href="javascript: void(0);">Event</a></li>
                                        <li class="breadcrumb-item active">Create</li>
                                    </ol>
                                </div>

                            </div>
                        </div>
                    </div>
                    <!-- end page title -->
                    @include('layouts.partials.alerts')
                    <div class="card">
                        <div class="card-header align-items-center d-flex">
                            <label><h5 class="card-title mb-0 flex-grow-1">Create</h5></label>
                            
                        </div>
                        <div class="card-body">
                            @if ($errors->any())
                                <div class="clearfix"></div>
                                <div class="alert alert-danger" role="alert">
                                    <a href="#" class="btn-close" data-bs-dismiss="alert" aria-label="Close">&times;</a>
                                    @foreach ($errors->all() as $error)
                                        <li>{{ $error }}</li>
                                    @endforeach
                                </div>
                            @endif
                            <form action="{{route('events.save')}}" method="POST" id="saveEvent" enctype="multipart/form-data">
                                @csrf
                                <div class="row">
                                    <div class="col-3">
                                        <div class="mb-3">
                                            <label for="Name" class="form-label">Name</label>
                                            <input type="text" name="name" id="name" class="form-control" placeholder="Enter Name" required>
                                        </div>
                                    </div><!--end col-->
                                    <div class="col-3">
                                        <div class="mb-3">
                                            <label for="event_type" class="form-label">Event Type</label>
                                            {{-- <input type="text" name="name" id="name" class="form-control" placeholder="Enter Name" required> --}}
                                            <select name="event_type" class="form-control">
                                                <option value="1">Academic</option>
                                                <option value="2">Non-Academic</option>
                                            </select>
                                        </div>
                                    </div><!--end col-->
                                    <div class="col-3">
                                        <div class="mb-3">
                                            <label for="expected_no_of_guests" class="form-label">Expected Guests</label>
                                            <input type="number" name="expected_no_of_guests" id="expected_no_of_guests" class="form-control" placeholder="Expected Guests" required>
                                        </div>
                                    </div><!--end col-->
                                    <div class="col-3">
                                        <div class="mb-3">
                                            <label for="expected_cost" class="form-label">Expected Cost</label>
                                            <input type="number" name="expected_cost" id="expected_cost" class="form-control" placeholder="Expected Cost" required>
                                        </div>
                                    </div><!--end col-->
                                    <div class="col-3">
                                        <div class="mb-3">
                                            <label for="max_capacity" class="form-label">Max Capacity</label>
                                            <input type="number" name="max_capacity" id="max_capacity" class="form-control" placeholder="Max Capacity" required>
                                        </div>
                                    </div><!--end col-->
                                    <div class="col-3">
                                        <div class="mb-3">
                                            <label for="min_capacity" class="form-label">Min Capacity</label>
                                            <input type="number" name="min_capacity" id="min_capacity" class="form-control" placeholder="Min Capacity" required>
                                        </div>
                                    </div><!--end col-->
                                    <div class="col-3">
                                        <div class="mb-3">
                                            <label for="date_from" class="form-label">Date From</label>
                                            <input type="date" name="date_from" id="date_from" class="form-control" placeholder="Date From" required>
                                        </div>
                                    </div><!--end col-->
                                    <div class="col-3">
                                        <div class="mb-3">
                                            <label for="date_to" class="form-label">Date To</label>
                                            <input type="date" name="date_to" id="date_to" class="form-control" placeholder="Date To" required>
                                        </div>
                                    </div><!--end col-->
                                    <div class="col-3">
                                        <div class="mb-3">
                                            <label for="image" class="form-label">Main Image</label>
                                            <input type="file" name="image" id="image" class="form-control" placeholder="Main Image" required>
                                        </div>
                                    </div><!--end col-->
                                    <div class="col-3">
                                        <div class="mb-3">
                                            <label for="additional_images" class="form-label">Additional Images</label>
                                            <input type="file" multiple name="additional_images[]" id="additional_images" class="form-control" placeholder="Additional Images" required>
                                        </div>
                                    </div><!--end col-->
                                    <div class="col-3">
                                        <div class="mb-3">
                                            <label for="lat" class="form-label">Latitude</label>
                                            <input type="text" name="lat" id="lat" class="form-control" placeholder="Latitude" required>
                                        </div>
                                    </div><!--end col-->
                                    <div class="col-3">
                                        <div class="mb-3">
                                            <label for="lng" class="form-label">Longitude</label>
                                            <input type="text" name="lng" id="lng" class="form-control" placeholder="Longitude" required>
                                        </div>
                                    </div><!--end col-->
                                    <div class="col-6">
                                        <div class="mb-3">
                                            <label for="tags" class="form-label">Tags</label>
                                            <textarea class="form-control" rows="6" cols="30" id="mainArticleBox" ></textarea>
                                            <input type="hidden" name="tags" id="tags">
                                            
                                        </div>
                                    </div>
                                    <div class="col-2 mt-3">
                                        <div id="DisplayArticle"></div>
                                    </div>
                                    <div class="col-4">
                                        <div class="mb-3">
                                            <label for="tags" class="form-label">Venue</label>
                                            <select name="venue_id" class="form-control">
                                                <option value="1">Auditorium</option>
                                                <option value="2">Library</option>
                                            </select>
                                            
                                        </div>
                                    </div>
                                    <div class="col-4 mt-3">
                                        <button type="submit" class="btn btn-primary">Save</button>
                                    </div>
                                </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    @endsection

    @section("js")
        <script>
            $(document).ready(function () {
                $("#mainArticleBox").keyup(function (e) {
                    var str = document.getElementById("mainArticleBox").value;
                    if (e.keyCode == 13) {
                        // Manipulate the DOM
                        var lines = str.split(/\r?\n/);
                        // create html
                        var html="";
                        var tagsArray = []; 
                        for(var i=0;i<lines.length;i++){
                            html+='<span class="badge bg-primary">'+lines[i]+'</span>';
                            tagsArray.push(lines[i]);
                        }
                        $("#tags").val(tagsArray);
                        // use html here showing inside some div
                        var divEl = document.getElementById("DisplayArticle");
                        divEl.innerHTML = html;
                    }
                });
            });
        </script>
    @endsection