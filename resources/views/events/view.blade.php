@extends('layouts.master')
        <!-- ========== App Menu ========== -->
        @section('navigation')
        @include('layouts.navigation')
        @endsection
        @section('css')
        <style>
        * {
            box-sizing: border-box;
          }
          
          .row > .column {
            padding: 0 8px;
          }
          
          .row:after {
            content: "";
            display: table;
            clear: both;
          }
          
          .column {
            float: left;
            width: 25%;
          }
          
          /* The Modal (background) */
          .modal {
            display: none;
            position: fixed;
            z-index: 1;
            padding-top: 100px;
            left: 0;
            top: 0;
            width: 100%;
            height: 100%;
            overflow: auto;
            background-color: black;
          }
          
          /* Modal Content */
          .modal-content {
            position: relative;
            background-color: #fefefe;
            margin: auto;
            padding: 0;
            width: 90%;
            max-width: 1200px;
          }
          
          /* The Close Button */
          .close {
            color: white;
            position: absolute;
            top: 10px;
            right: 25px;
            font-size: 35px;
            font-weight: bold;
          }
          
          .close:hover,
          .close:focus {
            color: #999;
            text-decoration: none;
            cursor: pointer;
          }
          
          .mySlides {
            display: none;
          }
          
          .cursor {
            cursor: pointer;
          }
          
          /* Next & previous buttons */
          .prev,
          .next {
            cursor: pointer;
            position: absolute;
            top: 50%;
            width: auto;
            padding: 16px;
            margin-top: -50px;
            color: white;
            font-weight: bold;
            font-size: 20px;
            transition: 0.6s ease;
            border-radius: 0 3px 3px 0;
            user-select: none;
            -webkit-user-select: none;
          }
          
          /* Position the "next button" to the right */
          .next {
            right: 0;
            border-radius: 3px 0 0 3px;
          }
          
          /* On hover, add a black background color with a little bit see-through */
          .prev:hover,
          .next:hover {
            background-color: rgba(0, 0, 0, 0.8);
          }
          
          /* Number text (1/3 etc) */
          .numbertext {
            color: #f2f2f2;
            font-size: 12px;
            padding: 8px 12px;
            position: absolute;
            top: 0;
          }
          
          img {
            margin-bottom: -4px;
          }
          
          .caption-container {
            text-align: center;
            background-color: black;
            padding: 2px 16px;
            color: white;
          }
          
          .demo {
            opacity: 0.6;
          }
          
          .active,
          .demo:hover {
            opacity: 1;
          }
          
          img.hover-shadow {
            transition: 0.3s;
          }
          
          .hover-shadow:hover {
            box-shadow: 0 4px 8px 0 rgba(0, 0, 0, 0.2), 0 6px 20px 0 rgba(0, 0, 0, 0.19);
          }
          </style>
        @endsection
        @section('content')
        <!-- Left Sidebar End -->
        <!-- Vertical Overlay-->
        <div class="vertical-overlay"></div>

        <!-- ============================================================== -->
        <!-- Start right Content here -->
        <!-- ============================================================== -->
        <div class="main-content">

            <div class="page-content">
                <div class="container-fluid">

                    <!-- start page title -->
                    <div class="row">
                        <div class="col-12">
                            <div class="page-title-box d-sm-flex align-items-center justify-content-between">
                                <h4 class="mb-sm-0">Event</h4>

                                <div class="page-title-right">
                                    <ol class="breadcrumb m-0">
                                        <li class="breadcrumb-item"><a href="javascript: void(0);">Event</a></li>
                                        <li class="breadcrumb-item active">View</li>
                                    </ol>
                                </div>

                            </div>
                        </div>
                    </div>
                    <!-- end page title -->
                    @include('layouts.partials.alerts')
                    <div class="card">
                        <div class="card-header align-items-center d-flex">
                            <label><h5 class="card-title mb-0 flex-grow-1">View</h5></label>
                            
                        </div>
                        <div class="card-body">
                            @if ($errors->any())
                                <div class="clearfix"></div>
                                <div class="alert alert-danger" role="alert">
                                    <a href="#" class="btn-close" data-bs-dismiss="alert" aria-label="Close">&times;</a>
                                    @foreach ($errors->all() as $error)
                                        <li>{{ $error }}</li>
                                    @endforeach
                                </div>
                            @endif
                            <form action="{{route('events.save')}}" method="POST" id="saveEvent" enctype="multipart/form-data">
                                @csrf
                                <div class="row">
                                    <div class="col-1">
                                        <label for="Name" class="form-label">Name</label>
                                    </div>
                                    <div class="col-2">
                                        <strong>{{$event->name}}</strong>
                                    </div>
                                    <div class="col-1">
                                        <label for="tags" class="form-label">Venue</label>
                                    </div>
                                    <div class="col-2">
                                            @if($event->event_type == 1)
                                            <strong>Auditorium</strong>
                                            @elseif($event->event_type == 2)
                                            <strong>Library</strong>
                                            @endif
                                    </div>
                                    <div class="col-1">
                                        <label for="event_type" class="form-label">Event Type</label>
                                    </div>
                                    <div class="col-2">
                                        @if($event->event_type == 1) 
                                        <strong>Academic</strong> 
                                        @elseif($event->event_type == 2)
                                        <strong>Non-Academic</strong>
                                        @endif
                                    </div><!--end col-->
                                    
                                    <div class="col-1">
                                        <label for="expected_no_of_guests" class="form-label">Expected Guests</label>
                                    </div>
                                    <div class="col-2">
                                    <strong>{{$event->expected_no_of_guests}}</strong>
                                    </div>
                                        
                                    <div class="col-1">
                                        <label for="expected_cost" class="form-label">Expected Cost</label>
                                    </div>
                                    <div class="col-2">
                                        <strong>{{$event->expected_cost}}</strong>
                                    </div><!--end col-->

                                    <div class="col-1">
                                        <label for="max_capacity" class="form-label">Max Capacity</label>
                                    </div>
                                    <div class="col-2">
                                        <strong>{{$event->max_capacity}}</strong>
                                    </div><!--end col-->
                                    <div class="col-1">
                                        <label for="min_capacity" class="form-label">Min Capacity</label>
                                    </div>
                                    <div class="col-2">
                                        <strong>{{$event->min_capacity}}</strong>                               
                                    </div><!--end col-->
                                    <div class="col-1">
                                        <label for="date_from" class="form-label">Date From</label>
                                    </div>
                                    <div class="col-2">
                                        <strong>{{date('d-M-Y', strtotime($event->date_from))}}</strong>
                                    </div><!--end col-->
                                    <div class="col-1">
                                        <label for="date_to" class="form-label">Date To</label>
                                    </div>
                                    <div class="col-2">
                                        <strong>{{date('d-M-Y', strtotime($event->date_to))}}</strong>
                                    </div><!--end col-->
                                    <hr>
                                    <div class="col-6">
                                        <label for="image" class="form-label">Main Image</label>
                                        <br>
                                        <img src="{{asset('/storage/uploads/events/' . $event->image . '')}}">
                                    </div>
                                    <div class="col-6">
                                        <label for="image" class="form-label">Additional Images</label>
                                        <br>
                                        @foreach($event->media as $img)
                                        <div class="column">
                                            <img src="{{asset('/storage/uploads/event_media/' . $img->name . '')}}" style="width:100%" onclick="openModal();currentSlide(1)" class="hover-shadow cursor">
                                        </div>
                                        @endforeach
                                        {{-- <img src="{{asset('/storage/uploads/event_media/' . $img->name . '')}}"> --}}
                                    </div><!--end col-->
                                   
                                    <div class="col-6">
                                        <div class="mb-3">
                                            <label for="tags" class="form-label">Tags</label>
                                            <br>
                                            @foreach($event->tags as $tag)
                                            <span class="badge bg-primary">{{$tag->name}}</span>
                                            @endforeach
                                            
                                        </div>
                                    </div>
                                    
                                    {{-- <div class="col-4 mt-3">
                                        <button type="submit" class="btn btn-primary">Save</button>
                                    </div> --}}
                                </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div id="myModal" class="modal">
            <span class="close cursor" onclick="closeModal()">&times;</span>
            <div class="modal-content">
                
            @foreach($event->media as $img)  
              <div class="mySlides">
                <div class="numbertext">1 / 4</div>
                <img src="{{asset('/storage/uploads/event_media/' . $img->name . '')}}" style="width:100%">
              </div>
            @endforeach
              <a class="prev" onclick="plusSlides(-1)">&#10094;</a>
              <a class="next" onclick="plusSlides(1)">&#10095;</a>
          
              <div class="caption-container">
                <p id="caption"></p>
              </div>
              @foreach($event->media as $img)
              <div class="column">
                <img class="demo cursor" src="{{asset('/storage/uploads/event_media/' . $img->name . '')}}" style="width:100%" onclick="currentSlide(1)">
              </div>
              @endforeach
              
            </div>
          </div>
    @endsection

    @section("js")
        <script>
            $(document).ready(function () {
                $("#mainArticleBox").keyup(function (e) {
                    var str = document.getElementById("mainArticleBox").value;
                    if (e.keyCode == 13) {
                        // Manipulate the DOM
                        var lines = str.split(/\r?\n/);
                        // create html
                        var html="";
                        var tagsArray = []; 
                        for(var i=0;i<lines.length;i++){
                            html+='<span class="badge bg-primary">'+lines[i]+'</span>';
                            tagsArray.push(lines[i]);
                        }
                        $("#tags").val(tagsArray);
                        // use html here showing inside some div
                        var divEl = document.getElementById("DisplayArticle");
                        divEl.innerHTML = html;
                    }
                });
            });
        </script>
    @endsection

    @section('js')
    <script>
        function openModal() {
          document.getElementById("myModal").style.display = "block";
        }
        
        function closeModal() {
          document.getElementById("myModal").style.display = "none";
        }
        
        var slideIndex = 1;
        showSlides(slideIndex);
        
        function plusSlides(n) {
          showSlides(slideIndex += n);
        }
        
        function currentSlide(n) {
          showSlides(slideIndex = n);
        }
        
        function showSlides(n) {
          var i;
          var slides = document.getElementsByClassName("mySlides");
          var dots = document.getElementsByClassName("demo");
          var captionText = document.getElementById("caption");
          if (n > slides.length) {slideIndex = 1}
          if (n < 1) {slideIndex = slides.length}
          for (i = 0; i < slides.length; i++) {
              slides[i].style.display = "none";
          }
          for (i = 0; i < dots.length; i++) {
              dots[i].className = dots[i].className.replace(" active", "");
          }
          slides[slideIndex-1].style.display = "block";
          dots[slideIndex-1].className += " active";
          captionText.innerHTML = dots[slideIndex-1].alt;
        }
        </script>
    @endsection