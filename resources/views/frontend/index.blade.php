<!doctype html>
<html lang="en" data-layout="vertical" data-layout-style="detached" data-sidebar="light" data-topbar="dark" data-sidebar-size="lg" data-sidebar-image="none">


<!-- Mirrored from themesbrand.com/velzon/html/interactive/nft-landing.html by HTTrack Website Copier/3.x [XR&CO'2014], Wed, 21 Sep 2022 07:40:16 GMT -->
<head>

    <meta charset="utf-8" />
    <title>SPMM | EMS</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta content="Premium Multipurpose Admin & Dashboard Template" name="description" />
    <meta content="Themesbrand" name="author" />
    <!-- App favicon -->
    <link rel="shortcut icon" href="{{asset('velzon/assets/images/favicon.ico')}}">

    <!--Swiper slider css-->
    <link href="{{asset('velzon/assets/libs/swiper/swiper-bundle.min.css')}}" rel="stylesheet" type="text/css" />

    <!-- Layout config Js -->
    <script src="{{asset('velzon/assets/js/layout.js')}}"></script>
    <!-- Bootstrap Css -->
    <link href="{{asset('velzon/assets/css/bootstrap.min.css')}}" rel="stylesheet" type="text/css" />
    <!-- Icons Css -->
    <link href="{{asset('velzon/assets/css/icons.min.css')}}" rel="stylesheet" type="text/css" />
    <!-- App Css-->
    <link href="{{asset('velzon/assets/css/app.min.css')}}" rel="stylesheet" type="text/css" />
    <!-- custom Css-->
    <link href="{{asset('velzon/assets/css/custom.min.css')}}" rel="stylesheet" type="text/css" />
    <style>
        * {
            box-sizing: border-box;
          }
          
          .row > .column {
            padding: 0 8px;
          }
          
          .row:after {
            content: "";
            display: table;
            clear: both;
          }
          
          .column {
            float: left;
            width: 25%;
          }
          
          /* The Modal (background) */
          
          .mySlides {
            display: none;
          }
          
          .cursor {
            cursor: pointer;
          }
          
          /* Next & previous buttons */
          .prev,
          .next {
            cursor: pointer;
            position: absolute;
            top: 50%;
            width: auto;
            padding: 16px;
            margin-top: -50px;
            color: white;
            font-weight: bold;
            font-size: 20px;
            transition: 0.6s ease;
            border-radius: 0 3px 3px 0;
            user-select: none;
            -webkit-user-select: none;
          }
          
          /* Position the "next button" to the right */
          .next {
            right: 0;
            border-radius: 3px 0 0 3px;
          }
          
          /* On hover, add a black background color with a little bit see-through */
          .prev:hover,
          .next:hover {
            background-color: rgba(0, 0, 0, 0.8);
          }
          
          /* Number text (1/3 etc) */
          .numbertext {
            color: #f2f2f2;
            font-size: 12px;
            padding: 8px 12px;
            position: absolute;
            top: 0;
          }
          
          img {
            margin-bottom: -4px;
          }
          
          .caption-container {
            text-align: center;
            background-color: black;
            padding: 2px 16px;
            color: white;
          }
          
          .demo {
            opacity: 0.6;
          }
          
          .active,
          .demo:hover {
            opacity: 1;
          }
          
          img.hover-shadow {
            transition: 0.3s;
          }
          
          .hover-shadow:hover {
            box-shadow: 0 4px 8px 0 rgba(0, 0, 0, 0.2), 0 6px 20px 0 rgba(0, 0, 0, 0.19);
          }
    </style>

</head>

<body data-bs-spy="scroll" data-bs-target="#navbar-example">

    <!-- Begin page -->
    <div class="layout-wrapper landing">
        <nav class="navbar navbar-expand-lg navbar-landing navbar-light fixed-top" id="navbar">
            <div class="container">
                <a class="navbar-brand" href="{{route('frontend.index')}}">
                    <h4 class="text-light">SPMM - EMS</h4>
                </a>
                <button class="navbar-toggler py-0 fs-20 text-body" type="button" data-bs-toggle="collapse" data-bs-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                    <i class="mdi mdi-menu"></i>
                </button>

                <div class="collapse navbar-collapse" id="navbarSupportedContent">
                    <ul class="navbar-nav mx-auto mt-2 mt-lg-0" id="navbar-example">
                        <li class="nav-item">
                            <a class="nav-link active" href="#hero">Home</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="#events">Events</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="#about">About</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="#howto">How to create event</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="#services">Services</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="#faq">FAQ</a>
                        </li>
                    </ul>

                    <div class="">
                        <a href="{{route('login')}}" class="btn btn-light">Login</a>
                    </div>
                </div>

            </div>
        </nav>
            <div class="bg-overlay bg-overlay-pattern"></div>
        <!-- end navbar -->

        <!-- start hero section -->
        <section class="section nft-hero" id="hero">
            <div class="bg-overlay"></div>
            <div class="container">
                <div class="row justify-content-center">
                    <div class="col-lg-8 col-sm-10">
                        <div class="text-center">
                            <h1 class="display-4 fw-medium mb-4 lh-base text-white">Event Management System <span class="text-success">FAST Events</span></h1>
                            <p class="lead text-white-50 lh-base mb-4 pb-2">Software Process Management & Metrics.</p>

                            <div class="hstack gap-2 justify-content-center">
                                <p class="lead text-white-50 lh-base mb-4 pb-2 muted">A project by company ABC.</p>
                            </div>
                        </div>
                    </div><!--end col-->
                </div><!-- end row -->
            </div><!-- end container -->
        </section><!-- end hero section -->

        <!-- start wallet -->
        <section class="section" id="events">
            <div class="container">
                <div class="row justify-content-center">
                    <div class="col-lg-8">
                        <div class="text-center mb-5">
                            <h2 class="mb-3 fw-semibold lh-base">Upcoming Events</h2>
                            <p class="text-muted">The list of upcoming official events of FAST NUCES. </p>
                        </div>
                    </div><!-- end col -->
                </div><!-- end row -->
                <div class="modal fade" id="fullscreeexampleModal" tabindex="-1" aria-labelledby="fullscreeexampleModalLabel" aria-hidden="true">
                    <div class="modal-dialog modal-fullscreen" >
                        <div class="modal-content">
                            <div class="modal-header">
                                <h5 class="modal-title" id="fullscreeexampleModalLabel">Event Data</h5>
                                <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                            </div>
                            <div class="modal-body row" id="viewEventDataModal">
                                
                            </div>
                            <div class="modal-footer">
                                <a href="javascript:void(0);" class="btn btn-link link-success fw-medium" data-bs-dismiss="modal"><i class="ri-close-line me-1 align-middle"></i> Close</a>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row g4">
                    @foreach($upcomingEvents as $event)
                    
                    <div class="col-lg-4">
                        <div class="card explore-box card-animate border">
                            <div class="card-body">
                                <div class="d-flex align-items-center mb-3">
                                    <div class="ms-2 flex-grow-1">
                                        <a href="#!"><h4 class="">{{$event->name}}</h4></a>
                                        <p class="mb-0 text-muted fs-13">
                                            {{date('d-M-Y', strtotime($event->date_from))}} - {{date('d-M-Y', strtotime($event->date_to))}}
                                        </p>
                                    </div>
                                    <div class="bookmark-icon">
                                        @if($event->status == 1)
                                        <span class="badge bg-success">Active</span>
                                        @elseif($event->status == 2)
                                        <span class="badge bg-danger">In-Active</span>
                                        @else
                                        <span class="badge bg-warning">N/A</span>
                                        @endif
                                    </div>
                                </div>
                                <div id="viewEvent" style="display: none;">
                                    <p>View Event</p>
                                </div>
                                <div class="explore-place-bid-img overflow-hidden rounded">
                                    <img src="{{asset('/storage/uploads/events/' . $event->image . '')}}" alt="" class="explore-img w-100">
                                    <div class="bg-overlay"></div>
                                    <div class="place-bid-btn">
                                        <a class="btn btn-primary" data-bs-toggle="modal" data-bs-target="#fullscreeexampleModal" onclick="viewEventModal({{$event->id}});"><i class="bx bxs-show"></i> View</a>
                                    </div>
                                </div>
                                <div class="mt-3">
                                    <h6 class="fs-16 mb-0">
                                        @if($event->event_type == 1) 
                                            <span class="badge bg-success">Academic</span>
                                            @elseif($event->event_type == 2)
                                            <span class="badge bg-primary">Non-Academic</span>
                                        @endif
                                    </h6>
                                </div>
                            </div>
                        </div>
                    </div>
                            <div class="modal-body row" id="eventDataInLoop{{$event->id}}" style="display: none;">
                            <div class="col-1">
                                <label for="Name" class="form-label">Name</label>
                            </div>
                            <div class="col-2">
                                <strong>{{$event->name}}</strong>
                            </div>
                            <div class="col-1">
                                <label for="tags" class="form-label">Venue</label>
                            </div>
                            <div class="col-2">
                                    @if($event->event_type == 1)
                                    <strong>Auditorium</strong>
                                    @elseif($event->event_type == 2)
                                    <strong>Library</strong>
                                    @endif
                            </div>
                            <div class="col-1">
                                <label for="event_type" class="form-label">Event Type</label>
                            </div>
                            <div class="col-2">
                                @if($event->event_type == 1) 
                                <strong>Academic</strong> 
                                @elseif($event->event_type == 2)
                                <strong>Non-Academic</strong>
                                @endif
                            </div><!--end col-->
                            
                            <div class="col-1">
                                <label for="expected_no_of_guests" class="form-label">Expected Guests</label>
                            </div>
                            <div class="col-2">
                            <strong>{{$event->expected_no_of_guests}}</strong>
                            </div>
                                
                            <div class="col-1">
                                <label for="expected_cost" class="form-label">Expected Cost</label>
                            </div>
                            <div class="col-2">
                                <strong>{{$event->expected_cost}}</strong>
                            </div><!--end col-->

                            <div class="col-1">
                                <label for="max_capacity" class="form-label">Max Capacity</label>
                            </div>
                            <div class="col-2">
                                <strong>{{$event->max_capacity}}</strong>
                            </div><!--end col-->
                            <div class="col-1">
                                <label for="min_capacity" class="form-label">Min Capacity</label>
                            </div>
                            <div class="col-2">
                                <strong>{{$event->min_capacity}}</strong>                               
                            </div><!--end col-->
                            <div class="col-1">
                                <label for="date_from" class="form-label">Date From</label>
                            </div>
                            <div class="col-2">
                                <strong>{{date('d-M-Y', strtotime($event->date_from))}}</strong>
                            </div><!--end col-->
                            <div class="col-1">
                                <label for="date_to" class="form-label">Date To</label>
                            </div>
                            <div class="col-2">
                                <strong>{{date('d-M-Y', strtotime($event->date_to))}}</strong>
                            </div><!--end col-->
                            <hr>
                            <div class="col-6">
                                <label for="image" class="form-label">Main Image</label>
                                <br>
                                <img src="{{asset('/storage/uploads/events/' . $event->image . '')}}">
                            </div>
                            <div class="col-6">
                                <label for="image" class="form-label">Additional Images</label>
                                <br>
                                @foreach($event->media as $img)
                                <div class="column">
                                    <img src="{{asset('/storage/uploads/event_media/' . $img->name . '')}}" style="width:100%" onclick="openModal();currentSlide(1)" class="hover-shadow cursor">
                                </div>
                                @endforeach
                                {{-- <img src="{{asset('/storage/uploads/event_media/' . $img->name . '')}}"> --}}
                            </div><!--end col-->
                        
                            <div class="col-6">
                                <div class="mb-3">
                                    <label for="tags" class="form-label">Tags</label>
                                    <br>
                                    @foreach($event->tags as $tag)
                                    <span class="badge bg-primary">{{$tag->name}}</span>
                                    @endforeach
                                    
                                </div>
                            </div>
                            </div>
                    @endforeach
                   
                </div><!--end row-->
            </div><!-- end container -->
        </section><!-- end wallet -->

        <section class="section" id="events">
            <div class="container">
                <div class="row justify-content-center">
                    <div class="col-lg-8">
                        <div class="text-center mb-5">
                            <h2 class="mb-3 fw-semibold lh-base">Past Events</h2>
                            <p class="text-muted">The list of past official events of FAST NUCES. </p>
                        </div>
                    </div><!-- end col -->
                </div><!-- end row -->
                <div class="modal fade" id="fullscreeexampleModal" tabindex="-1" aria-labelledby="fullscreeexampleModalLabel" aria-hidden="true">
                    <div class="modal-dialog modal-fullscreen" >
                        <div class="modal-content">
                            <div class="modal-header">
                                <h5 class="modal-title" id="fullscreeexampleModalLabel">Event Data</h5>
                                <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                            </div>
                            <div class="modal-body row" id="viewEventDataModal">
                                
                            </div>
                            <div class="modal-footer">
                                <a href="javascript:void(0);" class="btn btn-link link-success fw-medium" data-bs-dismiss="modal"><i class="ri-close-line me-1 align-middle"></i> Close</a>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row g4">
                    @foreach($pastEvents as $event)
                    
                    <div class="col-lg-4">
                        <div class="card explore-box card-animate border">
                            <div class="card-body">
                                <div class="d-flex align-items-center mb-3">
                                    <div class="ms-2 flex-grow-1">
                                        <a href="#!"><h4 class="">{{$event->name}}</h4></a>
                                        <p class="mb-0 text-muted fs-13">
                                            {{date('d-M-Y', strtotime($event->date_from))}} - {{date('d-M-Y', strtotime($event->date_to))}}
                                        </p>
                                    </div>
                                    <div class="bookmark-icon">
                                        @if($event->status == 1)
                                        <span class="badge bg-success">Active</span>
                                        @elseif($event->status == 2)
                                        <span class="badge bg-danger">In-Active</span>
                                        @else
                                        <span class="badge bg-warning">N/A</span>
                                        @endif
                                    </div>
                                </div>
                                <div id="viewEvent" style="display: none;">
                                    <p>View Event</p>
                                </div>
                                <div class="explore-place-bid-img overflow-hidden rounded">
                                    <img src="{{asset('/storage/uploads/events/' . $event->image . '')}}" alt="" class="explore-img w-100">
                                    <div class="bg-overlay"></div>
                                    <div class="place-bid-btn">
                                        <a class="btn btn-primary" data-bs-toggle="modal" data-bs-target="#fullscreeexampleModal" onclick="viewEventModal({{$event->id}});"><i class="bx bxs-show"></i> View</a>
                                    </div>
                                </div>
                                <div class="mt-3">
                                    <h6 class="fs-16 mb-0">
                                        @if($event->event_type == 1) 
                                            <span class="badge bg-success">Academic</span>
                                            @elseif($event->event_type == 2)
                                            <span class="badge bg-primary">Non-Academic</span>
                                        @endif
                                    </h6>
                                </div>
                            </div>
                        </div>
                    </div>
                            <div class="modal-body row" id="eventDataInLoop{{$event->id}}" style="display: none;">
                            <div class="col-1">
                                <label for="Name" class="form-label">Name</label>
                            </div>
                            <div class="col-2">
                                <strong>{{$event->name}}</strong>
                            </div>
                            <div class="col-1">
                                <label for="tags" class="form-label">Venue</label>
                            </div>
                            <div class="col-2">
                                    @if($event->event_type == 1)
                                    <strong>Auditorium</strong>
                                    @elseif($event->event_type == 2)
                                    <strong>Library</strong>
                                    @endif
                            </div>
                            <div class="col-1">
                                <label for="event_type" class="form-label">Event Type</label>
                            </div>
                            <div class="col-2">
                                @if($event->event_type == 1) 
                                <strong>Academic</strong> 
                                @elseif($event->event_type == 2)
                                <strong>Non-Academic</strong>
                                @endif
                            </div><!--end col-->
                            
                            <div class="col-1">
                                <label for="expected_no_of_guests" class="form-label">Expected Guests</label>
                            </div>
                            <div class="col-2">
                            <strong>{{$event->expected_no_of_guests}}</strong>
                            </div>
                                
                            <div class="col-1">
                                <label for="expected_cost" class="form-label">Expected Cost</label>
                            </div>
                            <div class="col-2">
                                <strong>{{$event->expected_cost}}</strong>
                            </div><!--end col-->

                            <div class="col-1">
                                <label for="max_capacity" class="form-label">Max Capacity</label>
                            </div>
                            <div class="col-2">
                                <strong>{{$event->max_capacity}}</strong>
                            </div><!--end col-->
                            <div class="col-1">
                                <label for="min_capacity" class="form-label">Min Capacity</label>
                            </div>
                            <div class="col-2">
                                <strong>{{$event->min_capacity}}</strong>                               
                            </div><!--end col-->
                            <div class="col-1">
                                <label for="date_from" class="form-label">Date From</label>
                            </div>
                            <div class="col-2">
                                <strong>{{date('d-M-Y', strtotime($event->date_from))}}</strong>
                            </div><!--end col-->
                            <div class="col-1">
                                <label for="date_to" class="form-label">Date To</label>
                            </div>
                            <div class="col-2">
                                <strong>{{date('d-M-Y', strtotime($event->date_to))}}</strong>
                            </div><!--end col-->
                            <hr>
                            <div class="col-6">
                                <label for="image" class="form-label">Main Image</label>
                                <br>
                                <img src="{{asset('/storage/uploads/events/' . $event->image . '')}}">
                            </div>
                            <div class="col-6">
                                <label for="image" class="form-label">Additional Images</label>
                                <br>
                                @foreach($event->media as $img)
                                <div class="column">
                                    <img src="{{asset('/storage/uploads/event_media/' . $img->name . '')}}" style="width:100%" onclick="openModal();currentSlide(1)" class="hover-shadow cursor">
                                </div>
                                @endforeach
                                {{-- <img src="{{asset('/storage/uploads/event_media/' . $img->name . '')}}"> --}}
                            </div><!--end col-->
                        
                            <div class="col-6">
                                <div class="mb-3">
                                    <label for="tags" class="form-label">Tags</label>
                                    <br>
                                    @foreach($event->tags as $tag)
                                    <span class="badge bg-primary">{{$tag->name}}</span>
                                    @endforeach
                                    
                                </div>
                            </div>
                            </div>
                    @endforeach
                   
                </div><!--end row-->
            </div><!-- end container -->
        </section><!-- end wallet -->

        <!-- start marketplace -->
        <section class="section bg-light" id="about">
            <div class="container">
                <div class="row justify-content-center">
                    <div class="col-lg-8">
                        <div class="text-center mb-5">
                            <h2 class="mb-3 fw-semibold lh-base">About EMS</h2>
                            <p class="text-muted mb-4">Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>
                            
                        </div>
                    </div><!-- end col -->
                </div><!-- end row -->
                
            </div><!-- end container -->
        </section>
        <!-- end marketplace -->

        <!-- start features -->
        <section class="section" id="howto">
            <div class="container">
                <div class="row justify-content-center">
                    <div class="col-lg-8">
                        <div class="text-center mb-5">
                            <h2 class="mb-3 fw-semibold lh-base">How to create event </h2>
                            <p class="text-muted">Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>
                        </div>
                    </div><!-- end col -->
                </div><!-- end row -->
                
                <div class="row">
                    <div class="col-lg-3">
                        <div class="card shadow-none">
                            <div class="card-body">
                                <img src="{{asset('velzon/assets/images/nft/wallet.png')}}" alt="" class="avatar-sm">
                                <h5 class="mt-4">Register as Admin</h5>
                                <p class="text-muted fs-14"></p>
                                <a href="#!" class="link-success fs-14">Read More <i class="ri-arrow-right-line align-bottom"></i></a>
                            </div>
                        </div>
                    </div><!--end col-->
                    <div class="col-lg-3">
                        <div class="card shadow-none">
                            <div class="card-body">
                                <img src="{{asset('velzon/assets/images/nft/money.png')}}" alt="" class="avatar-sm">
                                <h5 class="mt-4">Login</h5>
                                <p class="text-muted fs-14"></p>
                                <a href="#!" class="link-success fs-14">Read More <i class="ri-arrow-right-line align-bottom"></i></a>
                            </div>
                        </div>
                    </div><!--end col-->
                    <div class="col-lg-3">
                        <div class="card shadow-none">
                            <div class="card-body">
                                <img src="{{asset('velzon/assets/images/nft/add.png')}}" alt="" class="avatar-sm">
                                <h5 class="mt-4">Navigate to Events > Create</h5>
                                <p class="text-muted fs-14"></p>
                                <a href="#!" class="link-success fs-14">Read More <i class="ri-arrow-right-line align-bottom"></i></a>
                            </div>
                        </div>
                    </div><!--end col-->
                    <div class="col-lg-3">
                        <div class="card shadow-none">
                            <div class="card-body">
                                <img src="{{asset('velzon/assets/images/nft/sell.png')}}" alt="" class="avatar-sm">
                                <h5 class="mt-4">Fill in the form to create event</h5>
                                <p class="text-muted fs-14"></p>
                                <a href="#!" class="link-success fs-14">Read More <i class="ri-arrow-right-line align-bottom"></i></a>
                            </div>
                        </div>
                    </div><!--end col-->
                </div><!--end row-->
            </div><!-- end container -->
        </section><!-- end features -->

        <!-- start plan -->
        <section class="section bg-light" id="services">
            <div class="container-fluid">
                <div class="row justify-content-center">
                    <div class="col-lg-5">
                        <div class="text-center mb-5">
                            <h2 class="mb-3 fw-semibold lh-base">Services</h2>
                            <p class="text-muted">Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>
                        </div>
                    </div><!-- end col -->
                </div><!-- end row -->
                <div class="row">
                    <div class="col-lg-12">
                        <!-- Swiper -->
                        <div class="swiper mySwiper pb-4">
                            <div class="swiper-wrapper">
                                <div class="swiper-slide">
                                    <div class="card">
                                        <div class="card-body">
                                            <div class="row g-1 mb-3">
                                                <div class="col-lg-6">
                                                    <img src="{{asset('velzon/assets/images/nft/img-06.jpg')}}" alt="" class="img-fluid rounded">
                                                    <img src="{{asset('velzon/assets/images/nft/gif/img-2.gif')}}" alt="" class="img-fluid rounded mt-1">
                                                </div><!--end col-->
                                                <div class="col-lg-6">
                                                    <img src="{{asset('velzon/assets/images/nft/gif/img-5.gif')}}" alt="" class="img-fluid rounded mb-1">
                                                    <img src="{{asset('velzon/assets/images/nft/img-03.jpg')}}" alt="" class="img-fluid rounded">
                                                </div><!--end col-->
                                            </div><!--end row-->
                                            <a href="#!" class="float-end"> View All <i class="ri-arrow-right-line align-bottom"></i></a>
                                            <h5 class="mb-0 fs-16"><a href="#!" class="link-dark">MUNs <span class="badge badge-soft-success">206</span></a></h5>
                                        </div>
                                    </div>
                                </div>
                                <div class="swiper-slide">
                                    <div class="card">
                                        <div class="card-body">
                                            <div class="row g-1 mb-3">
                                                <div class="col-lg-6">
                                                    <img src="{{asset('velzon/assets/images/nft/img-05.jpg')}}" alt="" class="img-fluid rounded">
                                                    <img src="{{asset('velzon/assets/images/nft/gif/img-1.gif')}}" alt="" class="img-fluid rounded mt-1">
                                                </div><!--end col-->
                                                <div class="col-lg-6">
                                                    <img src="{{asset('velzon/assets/images/nft/gif/img-4.gif')}}" alt="" class="img-fluid rounded mb-1">
                                                    <img src="{{asset('velzon/assets/images/nft/img-04.jpg')}}" alt="" class="img-fluid rounded">
                                                </div><!--end col-->
                                            </div><!--end row-->
                                            <a href="#!" class="float-end"> View All <i class="ri-arrow-right-line align-bottom"></i></a>
                                            <h5 class="mb-0 fs-16"><a href="#!" class="link-dark">Tech competitions <span class="badge badge-soft-success">743</span></a></h5>
                                        </div>
                                    </div>
                                </div>
                                <div class="swiper-slide">
                                    <div class="card">
                                        <div class="card-body">
                                            <div class="row g-1 mb-3">
                                                <div class="col-lg-6">
                                                    <img src="{{asset('velzon/assets/images/nft/img-02.jpg')}}" alt="" class="img-fluid rounded">
                                                    <img src="{{asset('velzon/assets/images/nft/gif/img-3.gif')}}" alt="" class="img-fluid rounded mt-1">
                                                </div><!--end col-->
                                                <div class="col-lg-6">
                                                    <img src="{{asset('velzon/assets/images/nft/gif/img-1.gif')}}" alt="" class="img-fluid rounded mb-1">
                                                    <img src="{{asset('velzon/assets/images/nft/img-01.jpg')}}" alt="" class="img-fluid rounded">
                                                </div><!--end col-->
                                            </div><!--end row-->
                                            <a href="#!" class="float-end"> View All <i class="ri-arrow-right-line align-bottom"></i></a>
                                            <h5 class="mb-0 fs-16"><a href="#!" class="link-dark">Hackathons <span class="badge badge-soft-success">679</span></a></h5>
                                        </div>
                                    </div>
                                </div>
                                <div class="swiper-slide">
                                    <div class="card">
                                        <div class="card-body">
                                            <div class="row g-1 mb-3">
                                                <div class="col-lg-6">
                                                    <img src="{{asset('velzon/assets/images/nft/img-03.jpg')}}" alt="" class="img-fluid rounded">
                                                    <img src="{{asset('velzon/assets/images/nft/gif/img-5.gif')}}" alt="" class="img-fluid rounded mt-1">
                                                </div><!--end col-->
                                                <div class="col-lg-6">
                                                    <img src="{{asset('velzon/assets/images/nft/gif/img-2.gif')}}" alt="" class="img-fluid rounded mb-1">
                                                    <img src="{{asset('velzon/assets/images/nft/img-05.jpg')}}" alt="" class="img-fluid rounded">
                                                </div><!--end col-->
                                            </div><!--end row-->
                                            <a href="#!" class="float-end"> View All <i class="ri-arrow-right-line align-bottom"></i></a>
                                            <h5 class="mb-0 fs-16"><a href="#!" class="link-dark">Conferences <span class="badge badge-soft-success">341</span></a></h5>
                                        </div>
                                    </div>
                                </div>
                                <div class="swiper-slide">
                                    <div class="card">
                                        <div class="card-body">
                                            <div class="row g-1 mb-3">
                                                <div class="col-lg-6">
                                                    <img src="{{asset('velzon/assets/images/nft/img-02.jpg')}}" alt="" class="img-fluid rounded">
                                                    <img src="{{asset('velzon/assets/images/nft/gif/img-3.gif')}}" alt="" class="img-fluid rounded mt-1">
                                                </div><!--end col-->
                                                <div class="col-lg-6">
                                                    <img src="{{asset('velzon/assets/images/nft/gif/img-1.gif')}}" alt="" class="img-fluid rounded mb-1">
                                                    <img src="{{asset('velzon/assets/images/nft/img-01.jpg')}}" alt="" class="img-fluid rounded">
                                                </div><!--end col-->
                                            </div><!--end row-->
                                            <a href="#!" class="float-end"> View All <i class="ri-arrow-right-line align-bottom"></i></a>
                                            <h5 class="mb-0 fs-16"><a href="#!" class="link-dark">Annual Dinners <span class="badge badge-soft-success">1452</span></a></h5>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="swiper-pagination swiper-pagination-dark"></div>
                        </div>
                    </div>
                </div>
            </div><!-- end container -->
        </section>
        <!-- end plan -->

        <!-- start Discover Items-->
        <section class="section" id="faq">
            <div class="container">
                <div class="row">
                    <div class="col-lg-12">
                        <div class="card rounded-0 bg-soft-success mx-n4 mt-n4 border-top">
                            <div class="px-4">
                                <div class="row">
                                    <div class="col-xxl-5 align-self-center">
                                        <div class="py-4">
                                            <h4 class="display-6 coming-soon-text">Frequently asked questions</h4>
                                            <p class="text-success fs-15 mt-3">If you can not find answer to your question in our FAQ, you can always contact us or email us. We will answer you shortly!</p>
                                            <div class="hstack flex-wrap gap-2">
                                                <button type="button" class="btn btn-primary btn-label rounded-pill"><i class="ri-mail-line label-icon align-middle rounded-pill fs-16 me-2"></i> Email Us</button>
                                                <button type="button" class="btn btn-info btn-label rounded-pill"><i class="ri-twitter-line label-icon align-middle rounded-pill fs-16 me-2"></i> Send Us Tweet</button>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-xxl-3 ms-auto">
                                        <div class="mb-n5 pb-1 faq-img d-none d-xxl-block">
                                            <img src="assets/images/faq-img.png" alt="" class="img-fluid">
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!-- end card body -->
                        </div>
                        <!-- end card -->

                        <div class="row justify-content-evenly">
                            <div class="col-lg-4">
                                <div class="mt-3">
                                    <div class="d-flex align-items-center mb-2">
                                        <div class="flex-shrink-0 me-1">
                                            <i class="ri-question-line fs-24 align-middle text-info me-1"></i>
                                        </div>
                                        <div class="flex-grow-1">
                                            <h5 class="fs-16 mb-0 fw-semibold">General Questions</h5>
                                        </div>
                                    </div>

                                    <div class="accordion accordion-border-box" id="genques-accordion">
                                        <div class="accordion-item">
                                            <h2 class="accordion-header" id="genques-headingOne">
                                                <button class="accordion-button" type="button" data-bs-toggle="collapse" data-bs-target="#genques-collapseOne" aria-expanded="true" aria-controls="genques-collapseOne">
                                                    What is Lorem Ipsum ?
                                                </button>
                                            </h2>
                                            <div id="genques-collapseOne" class="accordion-collapse collapse" aria-labelledby="genques-headingOne" data-bs-parent="#genques-accordion">
                                                <div class="accordion-body">
                                                    If several languages coalesce, the grammar of the resulting language is more simple and regular than that of the individual languages. The new common language will be more simple and regular than the existing European languages. It will be as simple their most common words.
                                                </div>
                                            </div>
                                        </div>
                                        <div class="accordion-item">
                                            <h2 class="accordion-header" id="genques-headingTwo">
                                                <button class="accordion-button collapsed" type="button" data-bs-toggle="collapse" data-bs-target="#genques-collapseTwo" aria-expanded="false" aria-controls="genques-collapseTwo">
                                                    Why do we use it ?
                                                </button>
                                            </h2>
                                            <div id="genques-collapseTwo" class="accordion-collapse collapse" aria-labelledby="genques-headingTwo" data-bs-parent="#genques-accordion">
                                                <div class="accordion-body">
                                                    The new common language will be more simple and regular than the existing European languages. It will be as simple as Occidental; in fact, it will be Occidental. To an English person, it will seem like simplified English, as a skeptical Cambridge friend of mine told me what Occidental is.
                                                </div>
                                            </div>
                                        </div>
                                        <div class="accordion-item">
                                            <h2 class="accordion-header" id="genques-headingThree">
                                                <button class="accordion-button collapsed" type="button" data-bs-toggle="collapse" data-bs-target="#genques-collapseThree" aria-expanded="false" aria-controls="genques-collapseThree">
                                                    Where does it come from ?
                                                </button>
                                            </h2>
                                            <div id="genques-collapseThree" class="accordion-collapse collapse" aria-labelledby="genques-headingThree" data-bs-parent="#genques-accordion">
                                                <div class="accordion-body">
                                                    he wise man therefore always holds in these matters to this principle of selection: he rejects pleasures to secure other greater pleasures, or else he endures pains to avoid worse pains.But I must explain to you how all this mistaken idea of denouncing pleasure and praising pain was born and I will give you a complete.
                                                </div>
                                            </div>
                                        </div>
                                        <div class="accordion-item">
                                            <h2 class="accordion-header" id="genques-headingFour">
                                                <button class="accordion-button collapsed" type="button" data-bs-toggle="collapse" data-bs-target="#genques-collapseFour" aria-expanded="false" aria-controls="genques-collapseFour">
                                                    Where can I get some ?
                                                </button>
                                            </h2>
                                            <div id="genques-collapseFour" class="accordion-collapse collapse" aria-labelledby="genques-headingFour" data-bs-parent="#genques-accordion">
                                                <div class="accordion-body">
                                                    Cras ultricies mi eu turpis hendrerit fringilla. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; In ac dui quis mi consectetuer lacinia. Nam pretium turpis et arcu arcu tortor, suscipit eget, imperdiet nec, imperdiet iaculis aliquam ultrices mauris.
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <!--end accordion-->
                                </div>
                            </div>

                            <div class="col-lg-4">
                                <div class="mt-3">
                                    <div class="d-flex align-items-center mb-2">
                                        <div class="flex-shrink-0 me-1">
                                            <i class="ri-user-settings-line fs-24 align-middle text-info me-1"></i>
                                        </div>
                                        <div class="flex-grow-1">
                                            <h5 class="fs-16 mb-0 fw-semibold">Manage Account</h5>
                                        </div>
                                    </div>

                                    <div class="accordion accordion-border-box" id="manageaccount-accordion">
                                        <div class="accordion-item">
                                            <h2 class="accordion-header" id="manageaccount-headingOne">
                                                <button class="accordion-button collapsed" type="button" data-bs-toggle="collapse" data-bs-target="#manageaccount-collapseOne" aria-expanded="false" aria-controls="manageaccount-collapseOne">
                                                    Where can I get some ?
                                                </button>
                                            </h2>
                                            <div id="manageaccount-collapseOne" class="accordion-collapse collapse" aria-labelledby="manageaccount-headingOne" data-bs-parent="#manageaccount-accordion">
                                                <div class="accordion-body">
                                                    If several languages coalesce, the grammar of the resulting language is more simple and regular than that of the individual languages. The new common language will be more simple and regular than the existing European languages. It will be as simple their most common words.
                                                </div>
                                            </div>
                                        </div>
                                        <div class="accordion-item">
                                            <h2 class="accordion-header" id="manageaccount-headingTwo">
                                                <button class="accordion-button" type="button" data-bs-toggle="collapse" data-bs-target="#manageaccount-collapseTwo" aria-expanded="true" aria-controls="manageaccount-collapseTwo">
                                                    Where does it come from ?
                                                </button>
                                            </h2>
                                            <div id="manageaccount-collapseTwo" class="accordion-collapse collapse" aria-labelledby="manageaccount-headingTwo" data-bs-parent="#manageaccount-accordion">
                                                <div class="accordion-body">
                                                    The new common language will be more simple and regular than the existing European languages. It will be as simple as Occidental; in fact, it will be Occidental. To an English person, it will seem like simplified English, as a skeptical Cambridge friend of mine told me what Occidental is.
                                                </div>
                                            </div>
                                        </div>
                                        <div class="accordion-item">
                                            <h2 class="accordion-header" id="manageaccount-headingThree">
                                                <button class="accordion-button collapsed" type="button" data-bs-toggle="collapse" data-bs-target="#manageaccount-collapseThree" aria-expanded="false" aria-controls="manageaccount-collapseThree">
                                                    Why do we use it ?
                                                </button>
                                            </h2>
                                            <div id="manageaccount-collapseThree" class="accordion-collapse collapse" aria-labelledby="manageaccount-headingThree" data-bs-parent="#manageaccount-accordion">
                                                <div class="accordion-body">
                                                    he wise man therefore always holds in these matters to this principle of selection: he rejects pleasures to secure other greater pleasures, or else he endures pains to avoid worse pains.But I must explain to you how all this mistaken idea of denouncing pleasure and praising pain was born and I will give you a complete.
                                                </div>
                                            </div>
                                        </div>
                                        <div class="accordion-item">
                                            <h2 class="accordion-header" id="manageaccount-headingFour">
                                                <button class="accordion-button collapsed" type="button" data-bs-toggle="collapse" data-bs-target="#manageaccount-collapseFour" aria-expanded="false" aria-controls="manageaccount-collapseFour">
                                                    What is Lorem Ipsum ?
                                                </button>
                                            </h2>
                                            <div id="manageaccount-collapseFour" class="accordion-collapse collapse" aria-labelledby="manageaccount-headingFour" data-bs-parent="#manageaccount-accordion">
                                                <div class="accordion-body">
                                                    Cras ultricies mi eu turpis hendrerit fringilla. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; In ac dui quis mi consectetuer lacinia. Nam pretium turpis et arcu arcu tortor, suscipit eget, imperdiet nec, imperdiet iaculis aliquam ultrices mauris.
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <!--end accordion-->
                                </div>
                            </div>

                            <div class="col-lg-4">
                                <div class="mt-3">
                                    <div class="d-flex align-items-center mb-2">
                                        <div class="flex-shrink-0 me-1">
                                            <i class="ri-shield-keyhole-line fs-24 align-middle text-info me-1"></i>
                                        </div>
                                        <div class="flex-grow-1">
                                            <h5 class="fs-16 mb-0 fw-semibold">Privacy &amp; Security</h5>
                                        </div>
                                    </div>

                                    <div class="accordion accordion-border-box" id="privacy-accordion">
                                        <div class="accordion-item">
                                            <h2 class="accordion-header" id="privacy-headingOne">
                                                <button class="accordion-button" type="button" data-bs-toggle="collapse" data-bs-target="#privacy-collapseOne" aria-expanded="true" aria-controls="privacy-collapseOne">
                                                    Why do we use it ?
                                                </button>
                                            </h2>
                                            <div id="privacy-collapseOne" class="accordion-collapse collapse" aria-labelledby="privacy-headingOne" data-bs-parent="#privacy-accordion">
                                                <div class="accordion-body">
                                                    If several languages coalesce, the grammar of the resulting language is more simple and regular than that of the individual languages. The new common language will be more simple and regular than the existing European languages. It will be as simple their most common words.
                                                </div>
                                            </div>
                                        </div>
                                        <div class="accordion-item">
                                            <h2 class="accordion-header" id="privacy-headingTwo">
                                                <button class="accordion-button collapsed" type="button" data-bs-toggle="collapse" data-bs-target="#privacy-collapseTwo" aria-expanded="false" aria-controls="privacy-collapseTwo">
                                                    Where can I get some ?
                                                </button>
                                            </h2>
                                            <div id="privacy-collapseTwo" class="accordion-collapse collapse" aria-labelledby="privacy-headingTwo" data-bs-parent="#privacy-accordion">
                                                <div class="accordion-body">
                                                    The new common language will be more simple and regular than the existing European languages. It will be as simple as Occidental; in fact, it will be Occidental. To an English person, it will seem like simplified English, as a skeptical Cambridge friend of mine told me what Occidental is.
                                                </div>
                                            </div>
                                        </div>
                                        <div class="accordion-item">
                                            <h2 class="accordion-header" id="privacy-headingThree">
                                                <button class="accordion-button collapsed" type="button" data-bs-toggle="collapse" data-bs-target="#privacy-collapseThree" aria-expanded="false" aria-controls="privacy-collapseThree">
                                                    What is Lorem Ipsum ?
                                                </button>
                                            </h2>
                                            <div id="privacy-collapseThree" class="accordion-collapse collapse" aria-labelledby="privacy-headingThree" data-bs-parent="#privacy-accordion">
                                                <div class="accordion-body">
                                                    he wise man therefore always holds in these matters to this principle of selection: he rejects pleasures to secure other greater pleasures, or else he endures pains to avoid worse pains.But I must explain to you how all this mistaken idea of denouncing pleasure and praising pain was born and I will give you a complete.
                                                </div>
                                            </div>
                                        </div>
                                        <div class="accordion-item">
                                            <h2 class="accordion-header" id="privacy-headingFour">
                                                <button class="accordion-button collapsed" type="button" data-bs-toggle="collapse" data-bs-target="#privacy-collapseFour" aria-expanded="false" aria-controls="privacy-collapseFour">
                                                    Where does it come from ?
                                                </button>
                                            </h2>
                                            <div id="privacy-collapseFour" class="accordion-collapse collapse" aria-labelledby="privacy-headingFour" data-bs-parent="#privacy-accordion">
                                                <div class="accordion-body">
                                                    Cras ultricies mi eu turpis hendrerit fringilla. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; In ac dui quis mi consectetuer lacinia. Nam pretium turpis et arcu arcu tortor, suscipit eget, imperdiet nec, imperdiet iaculis aliquam ultrices mauris.
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <!--end accordion-->
                                </div>
                            </div>
                        </div>
                    </div>
                    <!--end col-->.
                </div>
            </div><!--end container-->
        </section>
        <!--end Discover Items-->

        <!-- start Work Process -->
        
        <!-- end cta -->

        <!-- Start footer -->
        <footer class="custom-footer bg-dark py-5 position-relative">
            <div class="container">
                <div class="row">
                    <div class="col-lg-4 mt-4">
                        <div>
                            <div>
                                {{-- <img src="{{asset('velzon/assets/images/logo-light.png')}}" alt="logo light" height="17"> --}}
                                <h4>SPMM - EMS</h4>
                            </div>
                            <div class="mt-4">
                                <p>Premium Events Management System</p>
                                <p>Software Process Management & Metrics.</p>
                            </div>
                        </div>
                    </div>

                    <div class="col-lg-7 ms-lg-auto">
                        <div class="row">
                            <div class="col-sm-4 mt-4">
                                <h5 class="text-white mb-0">Company</h5>
                                <div class="text-muted mt-3">
                                    <ul class="list-unstyled ff-secondary footer-list fs-14">
                                        <li><a href="#hero">Home</a></li>
                                        <li><a href="#about">About Us</a></li>
                                    </ul>
                                </div>
                            </div>
                            <div class="col-sm-4 mt-4">
                                <h5 class="text-white mb-0">Apps Pages</h5>
                                <div class="text-muted mt-3">
                                    <ul class="list-unstyled ff-secondary footer-list fs-14">
                                        <li><a href="#howto">How to</a></li>
                                        <li><a href="#services">Services</a></li>
                                    </ul>
                                </div>
                            </div>
                            <div class="col-sm-4 mt-4">
                                <h5 class="text-white mb-0">Support</h5>
                                <div class="text-muted mt-3">
                                    <ul class="list-unstyled ff-secondary footer-list fs-14">
                                        <li><a href="#faq">FAQ</a></li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>

                </div>

                <div class="row text-center text-sm-start align-items-center mt-5">
                    <div class="col-sm-6">

                        <div>
                            <p class="copy-rights mb-0">
                                <script> document.write(new Date().getFullYear()) </script> © SPMM - EMS
                            </p>
                        </div>
                    </div>
                    <div class="col-sm-6">
                        <div class="text-sm-end mt-3 mt-sm-0">
                            <ul class="list-inline mb-0 footer-social-link">
                                <li class="list-inline-item">
                                    <a href="javascript: void(0);" class="avatar-xs d-block">
                                        <div class="avatar-title rounded-circle">
                                            <i class="ri-facebook-fill"></i>
                                        </div>
                                    </a>
                                </li>
                                <li class="list-inline-item">
                                    <a href="javascript: void(0);" class="avatar-xs d-block">
                                        <div class="avatar-title rounded-circle">
                                            <i class="ri-github-fill"></i>
                                        </div>
                                    </a>
                                </li>
                                <li class="list-inline-item">
                                    <a href="javascript: void(0);" class="avatar-xs d-block">
                                        <div class="avatar-title rounded-circle">
                                            <i class="ri-linkedin-fill"></i>
                                        </div>
                                    </a>
                                </li>
                                <li class="list-inline-item">
                                    <a href="javascript: void(0);" class="avatar-xs d-block">
                                        <div class="avatar-title rounded-circle">
                                            <i class="ri-google-fill"></i>
                                        </div>
                                    </a>
                                </li>
                                <li class="list-inline-item">
                                    <a href="javascript: void(0);" class="avatar-xs d-block">
                                        <div class="avatar-title rounded-circle">
                                            <i class="ri-dribbble-line"></i>
                                        </div>
                                    </a>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </footer>
        <!-- end footer -->

    </div>
    <!-- end layout wrapper -->


    <!-- JAVASCRIPT -->
    <script src="{{asset('velzon/assets/libs/bootstrap/js/bootstrap.bundle.min.js')}}"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.6.1/jquery.min.js" integrity="sha512-aVKKRRi/Q/YV+4mjoKBsE4x3H+BkegoM/em46NNlCqNTmUYADjBbeNefNxYV7giUp0VxICtqdrbqU7iVaeZNXA==" crossorigin="anonymous" referrerpolicy="no-referrer"></script>
    <script src="{{asset('velzon/assets/libs/simplebar/simplebar.min.js')}}"></script>
    <script src="{{asset('velzon/assets/libs/node-waves/waves.min.js')}}"></script>
    <script src="{{asset('velzon/assets/libs/feather-icons/feather.min.js')}}"></script>
    <script src="{{asset('velzon/assets/js/pages/plugins/lord-icon-2.1.0.js')}}"></script>
    <script src="{{asset('velzon/assets/js/plugins.js')}}"></script>

    <!--Swiper slider js-->
    <script src="{{asset('velzon/assets/libs/swiper/swiper-bundle.min.js')}}"></script>

    <script src="{{asset('velzon/assets/js/pages/nft-landing.init.js')}}"></script>

    <script>
        function viewEventModal(eventId)
        {
            var eventData = $("#eventDataInLoop"+eventId).html();
            $("#viewEventDataModal").html(eventData);
        }

        
    </script>
    

</body>


<!-- Mirrored from themesbrand.com/velzon/html/interactive/nft-landing.html by HTTrack Website Copier/3.x [XR&CO'2014], Wed, 21 Sep 2022 07:40:17 GMT -->
</html>