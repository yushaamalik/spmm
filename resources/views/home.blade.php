@extends('layouts.master')
        <!-- ========== App Menu ========== -->
        @section('navigation')
        @include('layouts.navigation')
        @endsection
        @section('content')
        <div class="main-content">

            <div class="page-content">
                <div class="container-fluid">

                    <!-- start page title -->
                    <div class="row">
                        <div class="col-12">
                            <div class="page-title-box d-sm-flex align-items-center justify-content-between">
                                <h4 class="mb-sm-0">Home</h4>

                                <div class="page-title-right">
                                    <ol class="breadcrumb m-0">
                                        <li class="breadcrumb-item"><a href="javascript: void(0);">Admin</a></li>
                                        <li class="breadcrumb-item active">Home</li>
                                    </ol>
                                </div>

                            </div>
                            <div class="card">
                                <div class="card-header align-items-center d-flex">
                                    <h2 class="mb-0 fw-semibold lh-base flex-grow-1">Upcoming Events</h2>
                                    <a href="{{route('events.listing')}}" class="btn btn-primary">View All <i class="ri-arrow-right-line align-bottom"></i></a>
                                </div>
                                <div class="card-body">
                                    <section class="section" style="padding: 0;">
                                        <div class="container">
                                            <div class="row justify-content-center">
                                                <div class="col-lg-12">
                                                    <div class="d-flex align-items-center mb-5">
                                                    </div>
                                                </div>
                                            </div><!-- end row -->
                                            <div class="row">
                                                @foreach($upcomingEvents as $event)
                                                <div class="col-lg-4">
                                                    <div class="card explore-box card-animate border">
                                                        <div class="card-body">
                                                            <div class="d-flex align-items-center mb-3">
                                                                <div class="ms-2 flex-grow-1">
                                                                    <a href="#!"><h4 class="">{{$event->name}}</h4></a>
                                                                    <p class="mb-0 text-muted fs-13">
                                                                        {{date('d-M-Y', strtotime($event->date_from))}} - {{date('d-M-Y', strtotime($event->date_to))}}
                                                                    </p>
                                                                </div>
                                                                <div class="bookmark-icon">
                                                                    @if($event->status == 1)
                                                                    <span class="badge bg-success">Active</span>
                                                                    @elseif($event->status == 2)
                                                                    <span class="badge bg-danger">In-Active</span>
                                                                    @else
                                                                    <span class="badge bg-warning">N/A</span>
                                                                    @endif
                                                                </div>
                                                            </div>
                                                            <div class="explore-place-bid-img overflow-hidden rounded">
                                                                <img src="{{asset('/storage/uploads/events/' . $event->image . '')}}" alt="" class="explore-img w-100">
                                                                <div class="bg-overlay"></div>
                                                                <div class="place-bid-btn">
                                                                    <a href="{{route('events.view', ['id' => $event->id])}}" class="btn btn-primary"><i class="bx bxs-show"></i> View</a>
                                                                </div>
                                                            </div>
                                                            <div class="mt-3">
                                                                <h6 class="fs-16 mb-0">
                                                                    @if($event->event_type == 1) 
                                                                        <span class="badge bg-success">Academic</span>
                                                                        @elseif($event->event_type == 2)
                                                                        <span class="badge bg-primary">Non-Academic</span>
                                                                        @endif
                                                                </h6>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                @endforeach
                                               
                                            </div><!--end row-->
                                        </div><!--end container-->
                                    </section>
                                </div>
                            </div>


                            <div class="card">
                                <div class="card-header align-items-center d-flex">
                                    <h2 class="mb-0 fw-semibold lh-base flex-grow-1">Past Events</h2>
                                    <a href="{{route('events.listing')}}" class="btn btn-primary">View All <i class="ri-arrow-right-line align-bottom"></i></a>
                                    
                                </div>
                                <div class="card-body">
                                    <section class="section" style="padding: 0;">
                                        <div class="container">
                                            <div class="row justify-content-center">
                                                <div class="col-lg-12">
                                                    <div class="d-flex align-items-center mb-5">
                                                    </div>
                                                </div>
                                            </div><!-- end row -->
                                            <div class="row">
                                                @foreach($pastEvents as $event)
                                                <div class="col-lg-4">
                                                    <div class="card explore-box card-animate border">
                                                        <div class="card-body">
                                                            <div class="d-flex align-items-center mb-3">
                                                                <div class="ms-2 flex-grow-1">
                                                                    <a href="#!"><h4 class="">{{$event->name}}</h4></a>
                                                                    <p class="mb-0 text-muted fs-13">
                                                                        {{date('d-M-Y', strtotime($event->date_from))}} - {{date('d-M-Y', strtotime($event->date_to))}}
                                                                    </p>
                                                                </div>
                                                                <div class="bookmark-icon">
                                                                    @if($event->status == 1)
                                                                    <span class="badge bg-success">Active</span>
                                                                    @elseif($event->status == 2)
                                                                    <span class="badge bg-danger">In-Active</span>
                                                                    @else
                                                                    <span class="badge bg-warning">N/A</span>
                                                                    @endif
                                                                </div>
                                                            </div>
                                                            <div class="explore-place-bid-img overflow-hidden rounded">
                                                                <img src="{{asset('/storage/uploads/events/' . $event->image . '')}}" alt="" class="explore-img w-100">
                                                                <div class="bg-overlay"></div>
                                                                <div class="place-bid-btn">
                                                                    <a href="{{route('events.view', ['id' => $event->id])}}" class="btn btn-primary"><i class="bx bxs-show"></i> View</a>
                                                                </div>
                                                            </div>
                                                            <div class="mt-3">
                                                                <h6 class="fs-16 mb-0">
                                                                    @if($event->event_type == 1) 
                                                                        <span class="badge bg-success">Academic</span>
                                                                        @elseif($event->event_type == 2)
                                                                        <span class="badge bg-primary">Non-Academic</span>
                                                                        @endif
                                                                </h6>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                @endforeach
                                               
                                            </div><!--end row-->
                                        </div><!--end container-->
                                    </section>
                                </div>
                            </div>

                                
                            </div>
                        </div>
                    </div>
                    <!-- end page title -->

                </div>
                <!-- container-fluid -->
            </div>
    <!-- End Page-content -->

@endsection