<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Hash;
use App\Models\User;

class UsersSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        User::firstOrNew(['id' => 1, 'name' => 'Admin', 'email' => 'admin@admin.com', 'password' => Hash::make('password')]);
    }
}
