<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateEventsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('events', function (Blueprint $table) {
            $table->id();
            $table->integer('venue_id')->nullable()->default(0);
            $table->string('name')->nullable();
            $table->integer('event_type')->nullable()->default(1)->comment('1 for Academic, 2 for Non-Academic');
            $table->integer('expected_no_of_guests')->nullable()->default(0);
            $table->integer('expected_cost')->nullable()->default(0);
            $table->integer('max_capacity')->nullable()->default(0);
            $table->integer('min_capacity')->nullable()->default(0);
            $table->date('date_from')->nullable()->default(date('Y-m-d'));
            $table->date('date_to')->nullable()->default(date('Y-m-d'));
            $table->integer('tag_id')->nullable()->default(0);
            $table->string('image')->nullable()->default('Null');
            $table->integer('media_id')->nullable()->default(0);
            $table->string('lat')->nullable();
            $table->string('lng')->nullable();
            $table->integer('status')->nullable()->default(1)->comment('1 for Active, 2 for Non-Active');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('events');
    }
}
