<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use OwenIt\Auditing\Contracts\Auditable;

class Event extends Model implements Auditable
{
    use HasFactory;
    use \OwenIt\Auditing\Auditable;

    public function tags()
    {
        # code...
        return $this->hasMany('App\Models\EventTag', 'event_id', 'id');
    }

    public function media()
    {
        # code...
        return $this->hasMany('App\Models\EventMedia', 'event_id', 'id');

    }

    public function scopeActive($query)
    {
        # code...
        return $query->where('status', 1);
    }

}
