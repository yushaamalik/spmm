<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Event;
use App\Models\EventTag;
use App\Models\EventMedia;
use DB;
use Yajra\DataTables\Facades\DataTables;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        $upcomingEvents = Event::with(['tags', 'media'])
        ->where('date_to', '>=', date('Y-m-d'))
        ->orderBy('id', 'desc')
        ->Get();

        $pastEvents = Event::with(['tags', 'media'])
        ->where('date_to', '<', date('Y-m-d'))
        ->orderBy('id', 'desc')
        ->Get();

        return view('home', compact('upcomingEvents', 'pastEvents'));
    }
}
