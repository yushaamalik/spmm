<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Event;
use App\Models\EventTag;
use App\Models\EventMedia;
use DB;
use Yajra\DataTables\Facades\DataTables;

class FrontEndController extends Controller
{
    //
    public function index()
    {
        # code...
        $upcomingEvents = Event::with(['tags', 'media'])
        ->where('date_to', '>=', date('Y-m-d'))
        ->orderBy('id', 'desc')
        ->Get();

        $pastEvents = Event::with(['tags', 'media'])
        ->where('date_to', '<', date('Y-m-d'))
        ->orderBy('id', 'desc')
        ->Get();


        return view('frontend.index', compact('upcomingEvents', 'pastEvents'));
    }
    
}
