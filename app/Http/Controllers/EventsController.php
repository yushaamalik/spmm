<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Event;
use App\Models\EventTag;
use App\Models\EventMedia;
use DB;
use Yajra\DataTables\Facades\DataTables;

class EventsController extends Controller
{
    //

    public function listing(Request $request)
    {
        # code...
        $events = Event::with(['tags', 'media'])->orderBy('id', 'desc');
        if($request->ajax()){
            $data = Event::with(['tags', 'media'])->Active()->orderBy('id', 'desc');
            if($request->input('status'))
            {
                if($request->input('status') == 2)
                {
                    $data = Event::with(['tags', 'media'])->where(['status' => $request->input('status')])->orderBy('id', 'desc');
                }
                else{
                    $data->where(['status' => $request->input('status')]);
                }
            }
            if($request->input('date_from'))
            {
                $data->where(['date_from' => $request->input('date_from')]);
            }
            if($request->input('date_to'))
            {
                $data->where(['date_to' => $request->input('date_to')]);
            }

            $data->get();

            return DataTables::of($data)
            ->addIndexColumn()
            ->addColumn('name',function($data){

                $name = $data->name;
                return $name;
            })
            ->addColumn('date_from',function($data){
                $date_from = date('d-M-Y', strtotime($data->date_from));
                return $date_from;
            })
            ->addColumn('date_to',function($data){
                
                $date_to = date('d-M-Y', strtotime($data->date_to));
                return $date_to;
            })
            ->addColumn('event_type',function($data){
                // $event_type = $data->event_type;
                if($data->event_type == 1)
                {
                    $event_type = '<span class="badge bg-success">Academic</span>';
                }
                else if($data->event_type == 2)
                {
                    $event_type = '<span class="badge bg-primary">Non-Academic</span>';
                }

                return $event_type;
            })
            ->addColumn('action',function($data){

                $button  = '<div class="btn-group"><a href = "' . route('events.view', ['id' => $data->id]) . '" class="btn btn-sm btn-primary" title="View"><i class="bx bxs-show"></i></a>';
                $button .= '<a href = "' . route('events.edit', ['id' => $data->id]) . '" class="btn btn-sm btn-secondary" title="Edit"><i class="bx bx-edit-alt"></i></a></div>';
                $button .= '<a href = "' . route('events.changeStatus', ['id' => $data->id]) . '" class="btn btn-sm btn-warning" title="Change Status"><i class="bx bx-info-circle"></i></a></div>';
                return $button;
            })
            
            ->rawColumns(['name','date_from','date_to','event_type', 'action'])
            ->make(true);

        }

        return view('events.listing');

    }

    public function view($id)
    {
        # code...
        try{
            $event = Event::find($id);
            return view('events.view', compact('event'));
        }
        catch(\Exception $e){
            return redirect()->back()->with('error','Error. Please Contact Support');
        }
    }

    public function create()
    {
        # code...
        return view('events.create');
    }

    public function store(Request $request)
    {
        # code...
        $request->validate([
            'name' => 'required',
            'venue_id' => 'required',
            'event_type' => 'required',
            'expected_no_of_guests' => 'required',
            'expected_cost' => 'required',
            'max_capacity' => 'required',
            'min_capacity' => 'required',
            'date_from' => 'required',
            'date_to' => 'required',
            'image' => 'required',

        ]);
        try{    
            $tags = $request->input('tags');
            $tagsArray = explode (",", $tags); 

            if($request->hasFile('image')){
                //Getting file name with extension
                $fileNameWithExt = $request->file('image')->getClientOriginalName();
                //Get just file name
                $filename = pathinfo($fileNameWithExt, PATHINFO_FILENAME);
                //Get just ext
                $extension = $request->file('image')->getClientOriginalExtension();
                //Filename to store
                $fileNameToStore = $filename.'_'.time().'.'.$extension;
                //Upload Image
                $path = $request->file('image')->storeAs('public/uploads/events', $fileNameToStore);
                
    
    
            }
            else
            {
                $fileNameToStore = 'Null';
            }

            DB::beginTransaction();

            $event = new Event;

            $event->name                    = $request->input('name');
            $event->venue_id                = $request->input('venue_id');
            $event->event_type              = $request->input('event_type');
            $event->expected_no_of_guests   = $request->input('expected_no_of_guests');
            $event->expected_cost           = $request->input('expected_cost');
            $event->max_capacity            = $request->input('max_capacity');
            $event->min_capacity            = $request->input('min_capacity');
            $event->date_from               = $request->input('date_from');
            $event->date_to                 = $request->input('date_to');
            $event->lat                     = $request->input('lat');
            $event->lng                     = $request->input('lng');
            $event->image                   = $fileNameToStore;

            $event->save();

            foreach($tagsArray as $key => $tagVal)
            {
                $tag[$key]            = new EventTag;

                $tag[$key]->event_id  = $event->id;
                $tag[$key]->name      = $tagVal;

                $tag[$key]->save();
            }

            foreach($request->file('additional_images') as $key => $image)
            {
                $fileNameWithExt = $image->getClientOriginalName();
                //Get just file name
                $filename = pathinfo($fileNameWithExt, PATHINFO_FILENAME);
                //Get just ext
                $extension = $image->getClientOriginalExtension();
                //Filename to store
                $fileNameToStore = $filename.'_'.time().'.'.$extension;
                //Upload Image
                $path = $image->storeAs('public/uploads/event_media', $fileNameToStore);

                $eventMedia[$key]                     = new EventMedia;

                $eventMedia[$key]->event_id           = $event->id;
                $eventMedia[$key]->name               = $fileNameToStore;

                $eventMedia[$key]->save();


            }

            DB::commit();


            return redirect()->back()->with('success', 'Event added successfully');

        }
        catch(\Exception $e){
            return redirect()->back()->with('error','Error. Please Contact Support');
        }
    }

    public function changeStatus(Request $request, $id)
    {
        # code...
        try{

            $event = Event::find($id);

            if($event->status == 1)
            {
                $updateEvent = Event::where('id', $id)->update(['status' => 2]);
                return redirect()->back()->with('success', 'Event De-Activated successfully');
            }
            else if($event->status == 2)
            {
                $updateEvent = Event::where('id', $id)->update(['status' => 1]);
                return redirect()->back()->with('success', 'Event Activated successfully');
            }

        }
        catch(\Exception $e){
            return redirect()->back()->with('error','Error. Please Contact Support');
        }
    }

    public function edit($id)
    {
        # code...
        try{
            $event = Event::find($id);
            return view('events.edit', compact('event'));
        }
        catch(\Exception $e){
            return redirect()->back()->with('error','Error. Please Contact Support');
        }

    }

    public function update(Request $request, $id)
    {
        # code...
        $request->validate([
            'name' => 'required',
            'venue_id' => 'required',
            'event_type' => 'required',
            'expected_no_of_guests' => 'required',
            'expected_cost' => 'required',
            'max_capacity' => 'required',
            'min_capacity' => 'required',
            'date_from' => 'required',
            'date_to' => 'required',
            'image' => 'required',

        ]);
        try{

            if($request->hasFile('image')){
                //Getting file name with extension
                $fileNameWithExt = $request->file('image')->getClientOriginalName();
                //Get just file name
                $filename = pathinfo($fileNameWithExt, PATHINFO_FILENAME);
                //Get just ext
                $extension = $request->file('image')->getClientOriginalExtension();
                //Filename to store
                $fileNameToStore = $filename.'_'.time().'.'.$extension;
                //Upload Image
                $path = $request->file('image')->storeAs('public/uploads/events', $fileNameToStore);
    
            }
            else
            {
                $fileNameToStore = 'Null';
            }

            DB::beginTransaction();

            $event = Event::find($id);

            $event->name                    = $request->input('name');
            $event->venue_id                = $request->input('venue_id');
            $event->event_type              = $request->input('event_type');
            $event->expected_no_of_guests   = $request->input('expected_no_of_guests');
            $event->expected_cost           = $request->input('expected_cost');
            $event->max_capacity            = $request->input('max_capacity');
            $event->min_capacity            = $request->input('min_capacity');
            $event->date_from               = $request->input('date_from');
            $event->date_to                 = $request->input('date_to');
            $event->lat                     = $request->input('lat');
            $event->lng                     = $request->input('lng');
            $event->image                   = $fileNameToStore;

            $event->save();

            DB::commit();

            return redirect()->back()->with('success', 'Event data updated successfully.');
        }
        catch(\Exception $e){
            return redirect()->back()->with('error','Error. Please Contact Support');
        }
    }
}
